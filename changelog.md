# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]


## [0.4.0] - 2017-03-06

### Changed

- moving to tt-schiri: new license and changelog


## [0.3.0] - 2017-03-06

### Added

* more data, less statistics


## [0.2.0] - 2015-09-17

### Added

- Wins-Losses pie chart

### Fixed

- database for referees working


## [0.1.0] - 2015-09-17

### Added

- License, readme
- example data
- reading example data
- Live development in a line chart
